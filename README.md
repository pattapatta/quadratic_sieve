This project is a c++ implementation of the quadratic sieve algorithm for prime factorization, created by C.Pomerance.
To use the algorithm clone the project, create the lib and QS directory and then run make.
In tst/tse you will find tests for all classes, the actual quadratic sieve is in QS, to execute it just run ./QS/quadratic sieve .
Running ./tst/tse/semiprima x y  you will be able to generate a semiprime number.
Enjoy factoring!