#include "../include/qs_vector.h"

qs_vector::qs_vector(){
	weight = 0;
    leftmost_bit = 0;
    var_q=0;
    const_q=0;
}

qs_vector::qs_vector(unsigned long fb_dim, unsigned long A, mpz_class N){
	exp.clear();
	for(unsigned int i=0; i<fb_dim; ++i)
		exp.push_back(0);

	/*
		Now both const_q and var_q will be initialized with the value
        Q(A)=(floor(sqrt(N))+A)^2-N
	*/

    weight = 0;
    leftmost_bit = 0;
	var_q = poly_eval(A,N);
	const_q = poly_eval(A,N);
}

unsigned int qs_vector::size() const{
	return exp.size();
}

unsigned int qs_vector::operator[](int i){
	return exp[i];
}

unsigned int qs_vector::at(int i){
	return exp.at(i);
}

unsigned int qs_vector::at(int i) const{
	return (exp.at(i));
}

unsigned int qs_vector::get_leftmost_bit() const {
	return leftmost_bit;
}

unsigned long qs_vector::get_weight() const{
	return weight;
}

void qs_vector::calc_weight(){
  	unsigned long w = 0;
	for(unsigned int i=0; i < exp.size() ; ++i ){
		w += (exp[i])%2;
	}
	weight = w;
}

void qs_vector::set_exp(unsigned int index, int value){
	exp[index]=value;
	calc_weight();
}

void qs_vector::set_q(mpz_class value){
	var_q = value;
}

void qs_vector::set_weight(unsigned long w){
	weight = w;
}

void qs_vector::print(std::ostream& of) const{
	of << std::endl << "Q evaluation: " << const_q << std::endl;
	of << std::endl << "leftmost bit: " << leftmost_bit << std::endl;
	of << std::endl << "weight " << weight << std::endl;
	for(unsigned int i=0; i<exp.size(); ++i)
		of << std::setw(3) << exp[i];
}
/*
	This function return the Polynomial value
*/
mpz_class qs_vector::get_k_q() const{
	return const_q;
}

mpz_class qs_vector::get_v_q() const{
	return var_q;
}


/*
	The leftmost bit is the leftmost odd int in exp vector.
	It will be used during elimination part.
	everytime I set an exponent I have to check the leftmost_bit
	consistency: 
	  -if the actual index is minor than the leftmost bit and the 
	   exponent's value is odd I have to update leftmost_bit
 	  -if the actual index is equal to leftmost bit but the value is
 	   even I must set the leftmost bit to the nearest odd element on the
 	   right (major than the actual one)	
*/
void qs_vector::calc_leftmost_bit(){
	for(unsigned i=0; i<exp.size(); ++i)
		if(exp[i]%2==1){
			leftmost_bit=i;
			return;
		}
}

