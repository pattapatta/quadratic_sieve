#include "../include/factor_base.h"

factor_base::factor_base(mpz_class N, unsigned long ub){
	std::vector<unsigned long> erat = erathostene_sieve(ub);
	fb.push_back(2);
	mpz_class s = sqrt(N);
	
	mpz_class s1_2 = (s+1)%2;
	mpz_class s2_2 = (s+1)%2;
	sol ts;
	ts.s1 = s1_2.get_ui();
	ts.s2 = s2_2.get_ui();
	solutions.push_back(ts);
	for(unsigned int i=0; i<erat.size(); ++i){
		unsigned long p = erat[i];
		mpz_class pp(p);
		mpz_class m = N%pp;
		for(unsigned long j=0; 2*j<p; ++j){
			mpz_class jj(j);
			if(jj*jj%p==m){
				fb.push_back(p);
				sol sl;
				mpz_class t1=((pp - ((s - jj) % pp)) % pp);
				mpz_class t2=((pp - ((jj + s) % pp)) % pp);
				sl.s1 = t1.get_ui();
				sl.s2 = t2.get_ui();
				solutions.push_back(sl);
			}
		}
	}

}

void factor_base::print(std::ostream &of){
for(unsigned int i=0; i<fb.size(); ++i)
	of << std::setw(4) << fb[i] << std::setw(4) << " sol1 " << std::setw(4) << solutions[i].s1 << std::setw(4) << " sol2 " << std::setw(4) <<  solutions[i].s2 << std::endl;
}

unsigned long factor_base::fb_at(unsigned int i){
	return fb[i];
}

sol factor_base::sol_at(unsigned int i){
	return solutions[i];
}

unsigned long factor_base::size(){
	return fb.size();
}

std::ostream& operator<<(std::ostream &of, factor_base fb){
	fb.print(of);
	return of;
}
