#include "../include/qs_bin_vector.h"

qs_bin_vector::qs_bin_vector(const qs_vector &mv){
	const_q = mv.get_k_q();
	var_q = mv.get_k_q();
	weight = mv.get_weight();
    leftmost_bit = mv.get_leftmost_bit();

	//TODO check soundness of this function
	for(unsigned int i=0; i<mv.size(); ++i){
		if(mv.at(i)%2 == 0)
			exp_bin.push_back(false);
		else if(mv.at(i)%2==1)
			exp_bin.push_back(true);
	}
}


/*
	When I sum two binary vectors I will set also the right
	 weight, how? new weight will be the sum of vectors' weights
	 less w, which is the number of equal bit in the vectors
*/
void qs_bin_vector::sum(qs_bin_vector mvb){
	unsigned int w = 0;
	for(unsigned int i=0; i<mvb.size(); ++i){
		if((exp_bin[i]==mvb.exp_bin[i])&&exp_bin[i]==1)
			w+=2;
		exp_bin[i]=exp_bin[i]^mvb.exp_bin[i];	
	}
	weight = weight+mvb.get_weight()-w;
	calc_leftmost_bit();
}

bool qs_bin_vector::is_zero() const{
	return (weight==0);
}

unsigned int qs_bin_vector::size() const{
	return exp_bin.size();
}


void qs_bin_vector::print(std::ostream &of) const{
	of << std::endl << "Q evaluation: " << const_q << std::endl;
	of << std::endl << "leftmost bit: " << leftmost_bit << std::endl;
	of << std::endl << "weight is: " << weight << std::endl;
	for(unsigned int i=0; i<exp_bin.size(); ++i){
		of << std::setw(3) << exp_bin[i] ;
	}
}

unsigned long qs_bin_vector::get_weight() const{
	return weight;
}

bool qs_bin_vector::at(unsigned i){
	return exp_bin[i];
}


mpz_class qs_bin_vector::get_k_q() const{
	return const_q;
}

void qs_bin_vector::calc_leftmost_bit(){
	for(unsigned i=0; i<exp_bin.size(); ++i)
		if(exp_bin[i]%2==1){
			leftmost_bit=i;
			return;
		}
}

unsigned int qs_bin_vector::get_leftmost_bit(){
	return leftmost_bit;
}

std::ostream& operator<<(std::ostream& of, qs_bin_vector mvb){
	mvb.print(of);
	return of;
}

