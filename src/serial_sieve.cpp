 #include "../include/serial_sieve.h"



//! this function convert a qs_vector in a mpz_class 
mpz_class serial_sieve::convert(qs_vector mv){
	mpz_class n;
	n=1;
	for(unsigned int i=0; i<mv.size(); ++i)
		n=n*pow(fb.fb_at(i),mv.at(i));
	return n;
}

/*
	serial_sieve will initialize values and carry out the actual real sieving.
	It needs to know:
	  - how big the matrix of exponents must be (|Fb|+k), alias the mat_exp's 
	  	rows' number where k= dep_rel
	  - N, the number to factorize: array's elements are evaluated starting from
	    floor(sqrt(N))=s and Q(A)=(A+s)^2-N
	  - In how many point it has to evalue Q(A)
	  - the factor base
	    * factor base needs to know:  
	      (factor_base(mpz_class N, unsigned long ub))
	      - the upper bound ( Pomerance's function ), alias the qs_vector's vector
	        exp's columns' number, or the dimension of the factor base
	      - the number to sieve

	In this version we won't consider -1 like a prime, so we will consider a list of
	 Q(A) evaluation for A belonging to [0-M]

	That's the main function of the program, how does it work?
  	 -It set out a vector Q with values of polynomial Q(A) for a in range
	  [0-max_polyval]
	 -for each prime in factor base
	  - for each quadratic residue class of the prime
	    - for ( index i=square class; i<max_polyval; i=i+p)
	      - n = Q(i)
	      - while n%p==0
	        - n = n/p
		    - exponent_vector(i)(p)++;

TODO allow possibility to specify range for polynomial evaluation
*/	

serial_sieve::serial_sieve(qs_matrix &mat_exp, mpz_class N, unsigned int dep_rel, unsigned long min_polyval, unsigned long max_polyval, unsigned long ub)
		:fb(N,ub)
	{

	/*!
       We want to find |Fb|+k relations to be sure of linear dependency, dep_rel = k	
	   unsigned int dep_rel;
	   That's the matrix I will sieve on, after I'll have moved all B-smooth numbers's factorizations
	   from sieving_mat to mat_exp I will erase sieving_mat, in order to optimize memory usage
	*/
	std::vector<qs_vector> sieving_mat;

		//mat_exp.clear();
		for(unsigned long i=min_polyval; i<max_polyval; ++i){
			qs_vector t_vec(ub,i,N);
			sieving_mat.push_back(t_vec);
		}

	/*
	    Now we have something like that

	 	------------------------------------------
	 	|| k1 || v1 || 0 | 0 | 0 | 0 | | | | | 0 | 
	 	------------------------------------------
	 	|| k2 || v2 || 0 | 0 | 0 | 0 | | | | | 0 | 
	 	------------------------------------------
	 	|| k3 || v3 || 0 | 0 | 0 | 0 | | | | | 0 | 
		.									   .
		.									   .
		.									   .
	 	------------------------------------------
	 	|| km || vm || 0 | 0 | 0 | 0 | | | | | 0 | 
		------------------------------------------
		
		this matrix is sieving_mat, each row is a qs_vector, 
		each 0 in i-th position is the actual exponent of the i-th prime p 
		in v1 factorization. Both ki and vi are (initially) the polynomial's 
		evaluation values, when we will sieve we will divide vi for each p in 
		factor base till vi won't be 1.

	*/

		/*
			Here starts the actual sieve
			index i represent the i-th prime number
		*/
		for(unsigned int i=0; i<fb.size(); ++i){
			unsigned long p = fb.fb_at(i);
			unsigned long s1 = fb.sol_at(i).s1;
			unsigned long s2 = fb.sol_at(i).s2;
			for(unsigned long j=s1; j<max_polyval-min_polyval; j+=p){

				while(sieving_mat[j].get_v_q()%p==0){
					// I divide Q(A) for p till i can
					sieving_mat[j].set_q(sieving_mat[j].get_v_q()/p);
					// I increment exponent of p in Q(A) factorization
					sieving_mat[j].set_exp(i,sieving_mat[j].at(i)+1);
					//	sieving_mat[j].print(std::cout);
					std::cout << " first original: "  << std::setw(20) <<  sieving_mat[j].get_k_q() << " divided: " << std::setw(20) << sieving_mat[j].get_v_q() << std::endl;
				}
			}

			for(unsigned long j=s2; j<max_polyval-min_polyval; j+=p){
				while(sieving_mat[j].get_v_q()%p==0){
					// I divide Q(A) for p till i can
					sieving_mat[j].set_q(sieving_mat[j].get_v_q()/p);
					// I increment exponent of p in Q(A) factorization
					sieving_mat[j].set_exp(i,sieving_mat[j].at(i)+1);
					//sieving_mat[j].print(std::cout);
					std::cout << " second original: " << std::setw(20) << sieving_mat[j].get_k_q() << " divided: " << std::setw(20) << sieving_mat[j].get_v_q() << std::endl;
				}
			}

		}
		/*
			Here ends the actual sieve
		*/
	
		unsigned smooth_found = 0; // number of B-smooth number founded
		while(smooth_found<ub+dep_rel){
			for(unsigned int i=0; i<max_polyval-min_polyval; ++i)
				if(sieving_mat[i].get_v_q()==1){
					smooth_found++;
					// using those two function just here is
					// faster than call them for each vector, 'cause it
					// could be unseuseful
					sieving_mat[i].calc_weight();
					sieving_mat[i].calc_leftmost_bit();
					// I will set var_q to const q.
					// during gaussian elimination I will use var_q again
					sieving_mat[i].set_q(sieving_mat[i].get_k_q());
	 				sieving_mat[i].print(std::cout);
	 				mat_exp.push_back(sieving_mat[i]);
			}
			std::cout << "less than ub+dep_rel founded" << std::endl;
			break;
		}

		/*
			I will only push in matrix at least |Fb|+dep_rel
			 rows, so till bound < |Fb|+dep_rel I'll push
			 rows in mat_exp  
		*/
		/*
		unsigned bound = 0;
		for(unsigned int i=0; i<mat_exp.size(); ++i){
			if(bound<ub+dep_rel){
				mat_exp[i].print(std::cout);
				bound++;
				std::cout << std::endl << convert(mat_exp[i]) << std::endl;
			}
			else return;
		}
		*/
		// I must free the memory for sieving mat!! is it right?
		sieving_mat.clear();

	}



