#include "../../include/qs_bin_matrix.h"

/*
	qs_bin_matrix constructor will convert all matrix's 
	 my_vector in my_vector_bin, then will add them in its
	 own matrix.
	my_vector_bin can be build from a my_vector
*/
qs_bin_matrix::qs_bin_matrix(qs_matrix m){
	for(unsigned int i=0; i<m.size(); ++i){
		qs_bin_vector qbv(m[i]);
		bin_mat.push_back(qbv);
	}
	row = bin_mat.size();
	col = bin_mat[0].size();
}

qs_bin_vector qs_bin_matrix::operator[](unsigned int i){
	return bin_mat[i];
}

void qs_bin_matrix::print(std::ostream &of){
	for(unsigned int i=0; i<get_row(); ++i)
		bin_mat[i].print(of);
}

unsigned int qs_bin_matrix::size(){
	return bin_mat.size();
}

// this function sum two matrices' rows
void qs_bin_matrix::sum(int i, int j){
	// I'll use a xor (^) bit a bit operator 
	for(unsigned int k=0; k<bin_mat[0].size(); ++k)
		bin_mat[j].exp_bin[k]=bin_mat[j].exp_bin[k]^bin_mat[i].exp_bin[k];
}

void qs_bin_matrix::push_back(qs_bin_vector qbv){
	bin_mat.push_back(qbv);
}

unsigned long qs_bin_matrix::get_row(){
	return row;
}

unsigned long qs_bin_matrix::get_col(){
	return col;
}