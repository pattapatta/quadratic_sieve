#include "../../include/qs_matrix.h"


qs_matrix::qs_matrix(mpz_class N, unsigned long min_polyval, unsigned long max_polyval, unsigned long ub){
    mat.clear();
	for(unsigned long i=min_polyval; i<=max_polyval; ++i){
		qs_vector t_vec(ub,i,N);
		mat.push_back(t_vec);
	}
	row = mat.size();
	col = mat[0].size();
}

qs_vector qs_matrix::operator[](unsigned int i){
	return mat[i];
}

unsigned int qs_matrix::size(){
	return mat.size();
}

void qs_matrix::push_back(qs_vector qv){
	mat.push_back(qv);	
}

// maybe it's better using this sum instead of qs_vector's one.
void qs_matrix::sum(int i, int j){
	for(unsigned int k=0; k<get_col(); ++k)
		mat[j].set_exp(k,mat[j].at(k)+mat[i].at(k));
	mat[j].calc_leftmost_bit();
}

void qs_matrix::set_mat(unsigned i, unsigned j, int val){
	mat[i].set_exp(j,val);
}

unsigned long qs_matrix::get_row(){
	return row;
}

unsigned long qs_matrix::get_col(){
	return col;
}

void qs_matrix::print(std::ostream &of){
	for(unsigned int i=0; i<mat.size(); ++i)
		mat[i].print(of);
}


void qs_matrix::clear(){
	mat.clear();
}