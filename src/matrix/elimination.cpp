#include "../../include/elimination.h"


void elimination(qs_bin_matrix qbm, qs_matrix qm, std::vector<eli> &solutions ){
	std::cout << " ELIMINATION " << std::endl;
	//TODO Use assertions
	/*if(qbm.get_col()!=qm.get_col())
		//TODO an exception

	if(qbm.get_row()!=qm.get_row())
		//TOOO an exception
		
	*/	

	/*
		I'll find many null bin_vectors, so I'll save all the relative int ones
		  in this std::vector. 
	*/
	solutions.clear();
	/*
		l stands for the position of the leftmost bit
		j is just a generical index I'll use to scroll the matrix
	*/
	unsigned int l=0, j=0; 
	while(l<qbm.get_col()){
		/*
			I'm looking for the first row with the leftmost bit in position l, 
			 running from left to right.
			I'll use this bit as a pivot to delete all other row's bits in same position
		*/
		while(qbm[j].get_leftmost_bit()!=l && j<qbm.get_col())
			++j;
		std::cout << l << " 1 " << std::endl;
		/*
			When I've found such a row I will sum it with all others rows with 
			 the leftmost bit in same position
		*/
		for(unsigned int i=j+1; i<qbm.get_row(); ++i){
			if(qbm[i].get_leftmost_bit()==l){
				qm.sum(j,i);
				qbm.sum(j,i);
			}
			/*
				If a sum yelds to an empty row I'll return both the exponent vector
				 ( the integer one ) and the field var_q, in which I have the product
				 of all Q(A) evaluations.
				Those elements could lead to a factorization
			*/
			if(qbm[i].get_weight()==0){
				eli sol;
				sol.q=qm[i].get_v_q();
				sol.qsv=qm[i];
				solutions.push_back(sol);
			}
		}
		// I pass to the next position of leftmost bit
		++l;
	}

	return;
}