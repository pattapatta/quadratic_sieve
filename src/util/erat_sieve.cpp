#include "../../include/erat_sieve.h"

std::vector<unsigned long> erathostene_sieve(unsigned long ub){
   std::vector<unsigned long> erat(ub);

   for(unsigned int i=0; i<ub; i++)
     erat[i]=1;

   for(unsigned int i=2; i<ub; ++i)
     for(int j=2; i*j<ub; j++)
       erat[i*j]=0;

    std::vector<unsigned long> erat_t;
    for(unsigned int i=2; i<ub; i++)
      if(erat[i]!=0)
        erat_t.push_back(i);

  return erat_t;
}

