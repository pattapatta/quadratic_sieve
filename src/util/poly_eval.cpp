#include "../../include/poly_eval.h"

mpz_class poly_eval(unsigned int A, mpz_class N){
	mpz_class sq;
	mpf_class sqf;
	sqf = sqrt(N);
	sqf = ceil(sqf);
	sq = mpz_class(sqf);
	sq = sq+A;
	sq = sq*sq;
	sq = sq-N;
	return sq;
}
