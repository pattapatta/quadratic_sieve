
// file imported from dominick.direnzo@gmail.com
//  https://github.com/ddirenzo/qsieve
#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <time.h>

int main(int argc, char *argv[]) {
  gmp_randstate_t state;

  gmp_randinit_default(state);
  gmp_randseed_ui(state, time(NULL));

  int bitcnt0 = atoi(argv[1]);
  int bitcnt1 = atoi(argv[2]);

  mpz_t X, Y, LBX, LBY;

  mpz_init(X);
  mpz_init(Y);
  mpz_init_set_ui(LBX, 0);
  mpz_init_set_ui(LBY, 0);
  mpz_setbit(LBX, bitcnt0 - 1);
  mpz_setbit(LBY, bitcnt1 - 1);

  do {
    mpz_urandomb(X, state, bitcnt0);
  } while (mpz_cmp(X, LBX) < 0);
  mpz_nextprime(X, X);

  do {
    mpz_urandomb(Y, state, bitcnt1);
  } while (mpz_cmp(Y, LBY) < 0);
  mpz_nextprime(Y, Y);

  mpz_t N;
  mpz_init(N);
  mpz_mul(N, X, Y);

  gmp_printf("%Zd\n", N);
  mpz_clear(X);
  mpz_clear(Y);
  mpz_clear(LBX);
  mpz_clear(LBY);
  mpz_clear(N);
}