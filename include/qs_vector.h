#ifndef QS_VECTOR_GUARD_H
#define QS_VECTOR_GUARD_H

#include <vector>
#include <gmp.h>
#include <gmpxx.h>
#include <fstream>
#include <iomanip>
#include "poly_eval.h"

//TODO define a sum for qs_vector and an operator+ ( out of class )
/*!
 	\class qs_vector
	This class will provide us a vector composed by a normal std::vector
	 of exponents (int) and two more boxes where we will memorize the evaluation
	 of the quadratic polynomial
	That's not optimized.
*/
class qs_vector{
private:
	std::vector<int> exp;

	/*!
		I will use two elements in addiction to the exponent vector: const_q and var_q,
		 both will be set with the value of the polynomial in A, 
		 but during the sieving I will divide just var_q in order to set the exponents' 
		 vector, while const_q won't change its value no more.

		const_q is not defined const for comfort, it's private and there are no setter defined
		 to change it.
	*/
	mpz_class const_q;
	mpz_class var_q;

	/*!
	   Lookin ahead, we'll need to know the weight for binaries vectors, so I will compute it here.
	    I will add to weight each exponent mod2.
	*/
	unsigned long weight;


	//! this field will be usefull in linear albebra part
	unsigned int leftmost_bit;
   

public:
	qs_vector();
	/*!
		I will initialize qs_vector setting const_q and var_q
		 to Q(A)=(floor(sqrt(N))+A)^2-N and exp with a fb_dim-long 
		 vector of 0
	*/
	qs_vector(unsigned long fb_dim, unsigned long A, mpz_class N);
	unsigned int operator[](int i);
	unsigned int at(int i) const;
	unsigned int at(int i);
	unsigned int size() const;
    unsigned int get_leftmost_bit() const;
    unsigned long get_weight() const;
	//! this function will set var_q value
	void set_q(mpz_class value);
    void calc_weight();
	void set_exp(unsigned int index, int value);
	void set_weight(unsigned long w);
	void print(std::ostream& of) const;
	//! it set up the right value of leftmost_bit
	void calc_leftmost_bit();
	mpz_class get_k_q() const;
	mpz_class get_v_q() const;


};



#endif
