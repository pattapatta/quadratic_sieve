#ifndef QS_BIN_VECTOR_GUARD_H
#define QS_BIN_VECTOR_GUARD_H

#include <vector>
#include <gmpxx.h>
#include <fstream>
#include <iomanip>
#include "qs_vector.h"



//TODO check all const correctness

/*!
   \class qs_bin_vector 
*/

class qs_bin_vector{

private:
	//! That's the weight of bit set to 1 in the exponent
	unsigned long weight;
	/*!
	    both const_q and var_q, will be set with the value of the polynomial in A, 
		 but during the elimination I will multiply just var_q 
		 while const_q won't change its value no more.

		const_q is not defined const for comfort, it's private and there are no setter defined
		 to change it.
	*/
	mpz_class const_q;
	mpz_class var_q;

	/*! 
		this field will be usefull in linear albebra part,
		 it's the index of the position of the leftmost bit in the vector
	*/
	unsigned int leftmost_bit;

public:

	qs_bin_vector(const qs_vector &mv);
	//TODO set this private and define operator[] for this class
	std::vector<bool> exp_bin;
	bool at(unsigned i);
    //! returns true iff all vector's elements are 0, that is, the weight is 0
    bool is_zero() const;
    unsigned int size() const;
    unsigned int get_leftmost_bit();
    unsigned long get_weight() const;
	// sum is intended to be a binary sum, that is a xor bit a bit
    // I won't return a third vector, but I'll modify *this
    void sum(qs_bin_vector mvb);
    void print(std::ostream &of) const;
    void calc_leftmost_bit();
    mpz_class get_k_q() const;

};

std::ostream& operator<<(std::ostream& of, qs_bin_vector mvb);


#endif  //  QS_BIN_VECTOR_GUARD_H
