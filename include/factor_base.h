#ifndef FACTOR_BASE_GUARD_H
#define FACTOR_BASE_GUARD_H


#include <iostream>
#include <vector>
#include <gmp.h>
#include <gmpxx.h>
#include <cmath>
#include <iomanip>
#include <fstream>
#include "erat_sieve.h"

/*!
	This structure's aim is to contain solutions of equation
	 Q(A)=0 mod p (or equal, x^2=N mod p) for each prime p in the factor base.
	 s1 and s2 will be the roots 
*/
struct sol
{
	unsigned long s1;
	unsigned long s2;
};

/*!
  \class factor_base
  	This class provides us the factor base's vector and a vector containing 
	  congruences classes of squares modulo p.
  	Those classes will be usefull (necessary) when we'll go to sieve.
*/
class factor_base{
  private:
    /*!
        this vector contains the congruence classes 
        of square mod p
    */
    std::vector<sol> solutions; 
    /*!
        this vector contatins prime numbers under ub
        such that (N|p)=1
    */
    std::vector<unsigned long> fb; 
  public:
    /*!
        The constructor
        \param N [in]: the number we must factor
        \param ub [in]: the upper bound under wich we find primes numbers for the factor base
    */
  	factor_base(mpz_class N, unsigned long ub);
  	unsigned long size();
  	void print(std::ostream &of);
  	unsigned long fb_at(unsigned int i);
  	sol sol_at(unsigned int i);
};

std::ostream& operator<<(std::ostream& of, factor_base fb);


#endif