#ifndef POLY_EVAL_GUARD_H
#define POLY_EVAL_GUARD_H

#include <iostream>
#include <gmpxx.h>

/*!
	This function returns the evaluation of the polynomial Q(A)=(A+sqrt(N))^2-N 
	 for a certain A
	It will be useful to demand this work to a function, cause we can use other
	 polynomials for sieving
	\param A [in]: where we evaluate polynomial Q ( Q(A) )
	\param N [in]: number to be factored
	\return: the evaluation of Q(A)
*/

mpz_class poly_eval(unsigned int A, mpz_class N);


#endif 