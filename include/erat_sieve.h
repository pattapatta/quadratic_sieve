#ifndef ERATHOSTENE_SIEVE_GUARD_H
#define ERATHOSTENE_SIEVE_GUARD_H


#include <iostream>
#include <vector>

/*!
	this function implements the sieve of erathostene,
	\param ub [in]: upper bound under wich we find prime numbers for the factor base
	\return: a vector of prime numbers under ub
*/
std::vector<unsigned long> erathostene_sieve(unsigned long ub);


#endif