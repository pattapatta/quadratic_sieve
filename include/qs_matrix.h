#ifndef MATRIX_GUARD_H
#define MATRIX_GUARD_H

#include <vector>
#include <gmpxx.h>
#include <iostream>
#include <fstream>
#include "qs_vector.h"

/*!
	\class qs_matrix
 	\brief this class implements the matrix of exponent vectors

*/
class qs_matrix{

private:
 	std::vector<qs_vector> mat; 
	unsigned long row;  /*!< matrix' rows' number*/
	unsigned long col;  /*!< matrix' columns' number*/
public:

	//TODO eliminate this function, just for debugging
	void set_mat(unsigned i, unsigned j, int val);

	/*! 
		The constructor
		\param N [in]: the number we must factor
	  	\param max_polyval [in]: the range over we evaluate the polynomial Q
	  	\param ub [in]: the upper bound under wich we find primes numbers for the factor base
	*/
	qs_matrix(mpz_class N, unsigned long min_polyval, unsigned long max_polyval, unsigned long ub);
	qs_vector operator[](unsigned int i);
	void print(std::ostream &of);
	unsigned int size();
	/*! this function sum rows i and j and will put the result in j */
	void sum(int i, int j);
	void push_back(qs_vector qv);
	unsigned long get_row();
	unsigned long get_col();
	void clear();

};
#endif
