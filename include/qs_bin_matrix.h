#ifndef QS_BIN_MATRIX_GUARD_H
#define QS_BIN_MATRIX_GUARD_H

#include <iostream>
#include <vector>
#include <gmpxx.h>
#include <gmp.h>
#include "qs_matrix.h"
#include "qs_bin_vector.h"


/*! \class qs_bin_matrix

	After we've found at least |Fb|+k B-smooth factorizations 
	 we'll have to find a linear dependent combination of such vectors
	 in order to find a square.
	We'll use a customized binary vector, it will contain an exponent vector
	 and other elements in addiction:
	 - the value of Q(A); 
	 - the position of leftmost bit in each row
	 - the row's weight, that is the number of 1 in a row
	Gaussian elimination function will be define out of class
	 this class will be used just to create a binary qs_matrix from 
*/
class qs_bin_matrix{

private:
	//! That's the binary matrix 
	std::vector<qs_bin_vector> bin_mat;
	unsigned long row;
	unsigned long col;
public:

	/*!
		this is a copy constructor, it will create a new binary matrix
	  	 starting from a matrix of exponents.
	*/
    qs_bin_matrix(qs_matrix m);
	qs_bin_vector operator[](unsigned int i);
	unsigned int size();
	//! this function sum rows i and j and will put the result in j
	void sum(int i, int j);  
	void print(std::ostream &of);
	void push_back(qs_bin_vector qbv);  
	unsigned long get_row();
	unsigned long get_col();

};

#endif // QS_BIN_MATRIX_GUARD
