#ifndef SIEVE_GUARD_H
#define SIEVE_GUARD_H


#include <iostream>
#include <fstream>
#include <vector>
#include <gmp.h>
#include <gmpxx.h>
#include <iomanip>
#include <cmath>
#include "factor_base.h"
#include "qs_vector.h"
#include "poly_eval.h"
#include "qs_matrix.h"




class serial_sieve{
private:
	//! N is the number to factorize
    mpz_class N;
    //! ub is the upper bound for the factor base (L(b))
    //! it should be Pomerance's function
    unsigned long ub;
    /*!
       We want to find |Fb|+k relations to be sure of linear dependency, dep_rel = k	
	   unsigned int dep_rel;
	   That's the matrix I will sieve on, after I'll have moved all B-smooth numbers's factorizations
	   from sieving_mat to mat_exp I will erase sieving_mat, in order to optimize memory usage
	*/
	std::vector<qs_vector> sieving_mat;
  
	factor_base fb;

public:

	/*!
	   That's the matrix I will put only B-Smooth numbers with complete exponent vectors in
	    it's public because I'll need to use it to initialize the binary matrix
	    for gaussian elimination
		
	   \param mat_exp [out]: the integer matrix of exponent
	   \param N [in]: the number to be factored
	   \param dep_rel [in]: we have to find at least |Fb|+dep_rel relations to be
	   				   sure that some of them will be linear dependent
	   \param min_polyval [in]: minimunm of range of evaluation for Q
	   \param max_polyval [in]: maximum of range of evaluation for Q
	   \param ub [in]: upper bound for the factor base
	*/
	serial_sieve(qs_matrix &mat_exp, mpz_class N, unsigned int dep_rel, unsigned long min_polyval, unsigned long max_polyval, unsigned long ub);


	//! this function convert a qs_vector in a mpz_class 
   	mpz_class convert(qs_vector mv);

};




//TODO erase sieving_mat's rows when not necessaries

#endif
