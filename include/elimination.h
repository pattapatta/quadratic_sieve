#ifndef ELIMINATION_GUARD_H
#define ELIMINATION_GUARD_H

#include <iostream>
#include <gmpxx.h>
#include <vector>
#include "qs_matrix.h"
#include "qs_bin_matrix.h"
#include "qs_vector.h"

/*
	This class will provide us the gaussian elimination method,
	We'll need it, after we'll have found at least |FB|+k B-smooth
	 numbers, to find dependencies over exponent's vectors'.
	After we found a dependent subset of vector we'll have to check
	 if the product of Q(A_i) for i in I and the product of Q(A_i)mod N
	 lead to a nontrivial factorization for N
	
	How the elimination works?
	 let's consider the binary matrix first: we'll use the first vector
	 with the 1-leftmost bit to eliminate all other's vector's 1 in the same
	 position; each sum in binary matrix has to be done in the
	 exponent matrix, that is, we are summing exponents of vectors
	 and multiplying var_q of each vector.
    When we'll find a 0-row in binary matrix we will consider 
     var_q and exponent vector in the normal matrix, those two elements
     probably will lead to a nontrivial factorization

	What should this function return?
	 it could return an exponent vector and an mpz_class element,
	 
*/


/*!
	eli represents the return value of elimination().
	In order to obtain nontrivial factor of N we need 
	 -an exponent vector that leads to a perfect square coded in qsv
	 -and the product of Q(A1)Q(A2)...Q(An) that's coded in q
*/
struct eli{
	mpz_class q; /*!< q is the product of all Q(A) */
	qs_vector qsv;  /*!< qsv is the nonbinary vector that leads to a perfect square*/
};


/*!
	This function implements the Gaussian elimination over the
	 qs_bin_binary matrix and act the same operations over the 
	 qs_matrix. 
	\param qbm [in]: the binary matrix of exponent
	\param  qm [in]: the integer matrix of exponent
	\param solutions: a pair of an mpz_class and a vector
*/
void elimination(qs_bin_matrix qbm, qs_matrix qm,std::vector<eli> &solutions);


#endif