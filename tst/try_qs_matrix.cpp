#include <iostream>
#include <vector>
#include "../include/qs_matrix.h"

int main(){
    mpz_class N;
	N="12";
   	unsigned long max_polyval=5;
    unsigned long min_polyval=0;
	unsigned long ub=10;
	qs_matrix m(N,min_polyval,max_polyval,ub);
    m.print(std::cout);
    m.set_mat(0,0,12);
    m.set_mat(1,0,12);
    m.sum(0,1);
    std::cout << std::endl << std::endl;
    m.print(std::cout);

    m.set_mat(0,0,12);
    m.set_mat(1,0,12);

}
