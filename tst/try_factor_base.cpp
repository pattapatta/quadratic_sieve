#include <iostream>
#include <vector>
#include <gmpxx.h>
#include <gmp.h>
#include "../include/factor_base.h"

int main(){
	mpz_class N;
	N="8616460799";
	factor_base fb(N,103);
	std::cout << fb;

	std::cout << fb.fb_at(4) << std::endl << fb.sol_at(4).s1 << "   " << fb.sol_at(4).s2 << std::endl;
}