#include <iostream>
#include <gmpxx.h>
#include <string>
#include <cmath>
#include <unistd.h>
#include "../include/elimination.h"
#include "../include/qs_matrix.h"
#include "../include/qs_bin_matrix.h"
#include "../include/serial_sieve.h"


using namespace std;
/*
	This function implements the quadratic sieve.
	The algorithm works this way: given a number N
	 - creats a factor base of dimension fb=fb(N)
	 - act the serial_sieve.
	 - creates a binary matrix from the matrix modified 
	   by the serial sieve
	 - acts "elimination" on those matrices
	 - if elimination leads to a 0-weigth-row it will 
	   check if gcd(N,a-b) of gcd(N,a+b) with a=product of var_q
	   and b=convert(sol.qsv) are nontrivial factor of N
*/
int main(){
	string value;
	std::cout << "Insert N value " << std::endl; 
	cin >> value;
	mpz_class N(value);
	unsigned int ub;

	std::cout << "Insert upper bound for factor base" << std::endl;
	//std::cout << " pomerance approximation for " << N << " is " ;
	//std::cout << pomerance_approx(N) << std::endl;
	std::cin >> ub;
	factor_base fb(N,ub);
	
	std::cout << "Insert min polyval" << std::endl;
	unsigned long min_polyval;
	std::cin >> min_polyval;
	
	std::cout << "Insert max polyval" << std::endl;
	unsigned long max_polyval;
	std::cin >> max_polyval;
	
	std::cout << "Insert dep_rel" << std::endl; 
	unsigned int dep_rel;
	std::cin >> dep_rel;
    //Print matrix on file
	qs_matrix mat_exp(N, min_polyval, max_polyval, ub);
    std::cout << "mat_exp created" << std::endl;
    mat_exp.print(std::cout);
	serial_sieve sv(mat_exp, N, dep_rel, min_polyval, max_polyval, ub);
	std::cout << "serial sieve created" << std::endl;
	qs_bin_matrix bin_mat_exp(mat_exp);
    std::cout << "bin_mat_exp created" << std::endl;
	bin_mat_exp.print(std::cout);
	std::vector<eli> t;
	elimination(bin_mat_exp,mat_exp,t);


    /*
		Now I have a vector of x and y where x are the product of Q(A)
		 and y are int vector.
		I have to look for a nontrivial factorization for N, that is, 
		 till gcd(x-y,N)==1 || gcd(x-y,N)==N I must go on looking for another
		 solution
    */
	
    for(unsigned i=0; i<t.size(); ++i){
    	mpz_class b = sv.convert(t[i].qsv);
		mpz_class a = t[i].q;

		mpz_class f1 = abs(a-b);
		mpz_class f2 = a+b;

		mpz_class g1 = gcd(f1,N);
    	mpz_class g2 = gcd(f2,N);
  	

    	if(g1!=1 && g1!=N){
    		std::cout << "first factor: " << g1 << std::endl;
			std::cout << "second factor: " << N/g1 << std::endl;
			return 0;
		}

		if(g2!=1 && g1!=N){
			std::cout << "first factor: " << g2 << std::endl;
			std::cout << "second factor: " << N/g2 << std::endl;
			return 0;
		}

		
    }

    std::cout << " No nontrivial factorization found " << std::endl;
    return 0;



//TODO restart computation if factor are 1 and N
}