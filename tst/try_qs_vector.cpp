#include <vector>
#include <iostream>
#include "../include/qs_vector.h"

int main(){
	mpz_class N;
	N="8616460799";
	unsigned long fb_dim = 5;
	unsigned int A = 5;
	mpz_class T = poly_eval(A,N);
	// my should be long 5
	qs_vector my(fb_dim, A, N);
	std::cout << my.size() << std::endl;
	my.print(std::cout);
	my.set_exp(3,12);
}
