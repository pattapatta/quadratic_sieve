#include <iostream>
#include "../include/qs_bin_matrix.h"
#include "../include/qs_matrix.h"

int main(){
    mpz_class N;
	N="12";
	unsigned long min_polyval=0;
   	unsigned long max_polyval=5;
	unsigned long ub=10;
	qs_matrix m(N,min_polyval,max_polyval,ub);
	m.print(std::cout);


    std::cout << std::endl;
	qs_bin_matrix bm(m);
    std::cout << std::endl; 		
	bm.print(std::cout);

}