#include "../include/qs_bin_vector.h"
#include "../include/qs_vector.h"
#include <vector>
#include <iostream>
#include <gmpxx.h>

int main(){
	mpz_class N;
	N="8616460799";
	unsigned long fb_dim = 5;
	unsigned int A = 5;
	mpz_class T = poly_eval(A,N);
	
    std::cout << "*********************" << std::endl;
    std::cout << "* My vector creation*" << std::endl;
    std::cout << "*********************" << std::endl;
	qs_vector my(fb_dim, A, N);
    my.print(std::cout);
    std::cout << "*********************" << std::endl;
    std::cout << "* set exponents     *" << std::endl;
    std::cout << "*********************" << std::endl;
    my.set_exp(1,2);
    my.set_exp(2,1);
    my.print(std::cout);
    std::cout << "*********************" << std::endl;
    std::cout << "* creation of binary*" << std::endl;
    std::cout << "*********************" << std::endl;
    qs_bin_vector mvb(my);
    mvb.print(std::cout);
    std::cout << "*********************" << std::endl;
    std::cout << "*   change my exp   *" << std::endl;
    std::cout << "*********************" << std::endl;
    my.set_exp(3,1);
    my.print(std::cout);
    std::cout << "*********************" << std::endl;
    std::cout << "* creation of binary*" << std::endl;
    std::cout << "*********************" << std::endl;
    qs_bin_vector mvb2(my);
    mvb2.print(std::cout);
    std::cout << "*********************" << std::endl;
    std::cout << "*  sum 2 bin vec    *" << std::endl;
    std::cout << "*********************" << std::endl;
    std::cout << " first " << std::endl;
    mvb.print(std::cout);
    std::cout << " second " << std::endl;
    mvb2.print(std::cout);
    std::cout << " sum " << std::endl;
    mvb.sum(mvb2);
    mvb.print(std::cout);

    std::cout << "*********************" << std::endl;
    std::cout << "*  MORE COMPLEX     *" << std::endl;
    std::cout << "*********************" << std::endl;
    unsigned long fb_dim2 = 50;
    unsigned int A2 = 50;
    mpz_class T2 = poly_eval(A2,N);

    std::cout << "*********************" << std::endl;
    std::cout << "* My vector creation*" << std::endl;
    std::cout << "*********************" << std::endl;
    qs_vector my2(fb_dim2, A2, N);
    my2.print(std::cout);
    std::cout << "*********************" << std::endl;
    std::cout << "* set exponents     *" << std::endl;
    std::cout << "*********************" << std::endl;
    my2.set_exp(1,1);
    my2.set_exp(2,2);
    my2.set_exp(3,3);
    my2.set_exp(4,4);
    my2.set_exp(5,5);
    my2.set_exp(6,6);
    my2.set_exp(7,7);
    my2.set_exp(8,8);
    my2.set_exp(9,9);
    my2.set_exp(10,10);
    my2.print(std::cout);
    std::cout << "*********************" << std::endl;
    std::cout << "* creation of binary*" << std::endl;
    std::cout << "*********************" << std::endl;
    qs_bin_vector mvb3(my2);
    mvb3.print(std::cout);
    std::cout << "*********************" << std::endl;
    std::cout << "* creation of binary (same as before)*" << std::endl;
    std::cout << "*********************" << std::endl;
    qs_bin_vector mvb4(my2);
    mvb4.print(std::cout);
    std::cout << "*********************" << std::endl;
    std::cout << "*  sum 2 bin vec    *" << std::endl;
    std::cout << "*********************" << std::endl;
    std::cout << " first " << std::endl;
    mvb4.print(std::cout);
    std::cout << " second " << std::endl;
    mvb3.print(std::cout);
    std::cout << " sum " << std::endl;
    mvb4.sum(mvb3);
    mvb4.print(std::cout);

}
