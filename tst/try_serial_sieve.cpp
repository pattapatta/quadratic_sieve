#include <iostream>
#include "../include/serial_sieve.h"


int main(){


	mpz_class N;
	N="8616460799";
	unsigned long fb_dim = 5;
	unsigned int A = 5;
	mpz_class T = poly_eval(A,N);
	qs_vector my(fb_dim, A, N); 	
	my.print(std::cout);

	std::cout << std::endl << "***************" << std::endl;

	unsigned int dep_rel = 100;	
	unsigned long min_polyval=0;
	unsigned long max_polyval=500;
	unsigned long ub=1000;
	qs_matrix qm(N,min_polyval,max_polyval,ub);
	serial_sieve sv (qm,N, dep_rel, min_polyval, max_polyval, ub);
}
