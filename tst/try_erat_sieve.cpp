#include <iostream>
#include <vector>
#include "../include/erat_sieve.h"

int main(){

  std::vector<unsigned long> v = erathostene_sieve(1000000);
  for(unsigned int i=0; i<v.size(); ++i)
   std::cout << v[i] << std::endl;

}
