CC=g++
GMP= -lgmpxx -lgmp 
SRC=src
DBG=-g
EXE=exe 
LIB=lib
INC=include
UTL=src/util
MAT=src/matrix
TST=tst
TSE=tst/tst_exe
ERR=-Wall -Wextra
VER=-std=c++11

#TODO recursive dependencies

lib= $(LIB)/poly_eval.o $(LIB)/erat_sieve.o $(LIB)/qs_vector.o $(LIB)/factor_base.o $(LIB)/serial_sieve.o $(LIB)/qs_bin_vector.o $(LIB)/qs_matrix.o $(LIB)/qs_bin_matrix.o $(LIB)/elimination.o 

tst= $(TSE)/try_erat_sieve $(TSE)/try_factor_base $(TSE)/try_serial_sieve $(TSE)/try_qs_bin_vector $(TSE)/try_qs_matrix $(TSE)/try_qs_vector $(TSE)/try_qs_bin_matrix $(TSE)/quadratic_sieve

all: $(lib) $(tst)  semiprime


semiprime:
	$(CC) $(CFLAGS) -O2  $(UTL)/semiprime.cc $(GMP) -pthread -o $(TSE)/semiprime


$(LIB)/erat_sieve.o: $(UTL)/erat_sieve.cpp 
	$(CC) -c $(ERR) $(UTL)/erat_sieve.cpp -o $(LIB)/erat_sieve.o

$(LIB)/serial_sieve.o: $(SRC)/serial_sieve.cpp $(INC)/qs_vector.h $(INC)/factor_base.h
	$(CC) -c $(ERR) $(SRC)/serial_sieve.cpp -o $(LIB)/serial_sieve.o

$(LIB)/factor_base.o: $(SRC)/factor_base.cpp $(INC)/erat_sieve.h
	$(CC) -c $(ERR) $(SRC)/factor_base.cpp -o $(LIB)/factor_base.o

$(LIB)/qs_vector.o: $(SRC)/qs_vector.cpp $(UTL)/poly_eval.cpp
	$(CC) -c $(ERR) $(SRC)/qs_vector.cpp -o $(LIB)/qs_vector.o

$(LIB)/poly_eval.o: $(UTL)/poly_eval.cpp
	$(CC) -c $(ERR) $(UTL)/poly_eval.cpp -o $(LIB)/poly_eval.o $(GMP)

$(LIB)/qs_bin_vector.o: $(SRC)/qs_bin_vector.cpp $(INC)/qs_vector.h
	$(CC) -c $(ERR) $(SRC)/qs_bin_vector.cpp -o $(LIB)/qs_bin_vector.o $(GMP)

$(LIB)/qs_matrix.o: $(MAT)/qs_matrix.cpp $(INC)/qs_vector.h
	$(CC) -c $(ERR) $(MAT)/qs_matrix.cpp -o $(LIB)/qs_matrix.o $(GMP)

$(LIB)/qs_bin_matrix.o: $(MAT)/qs_bin_matrix.cpp $(INC)/qs_bin_vector.h $(INC)/qs_matrix.h
	$(CC) -c $(ERR) $(MAT)/qs_bin_matrix.cpp -o $(LIB)/qs_bin_matrix.o $(GMP)

$(LIB)/elimination.o:  $(MAT)/elimination.cpp $(INC)/qs_bin_matrix.h $(INC)/qs_bin_vector.h $(INC)/qs_matrix.h
	$(CC) -c $(ERR) $(MAT)/elimination.cpp -o $(LIB)/elimination.o $(GMP)

# $(LIB)/pomerance_approx.o: $(UTL)/pomerance_approx.cpp
# 	$(CC) -c $(ERR) $(UTL)/pomerance_approx.cpp -o $(LIB)/pomerance_approx.o


$(TSE)/try_factor_base: $(TST)/try_factor_base.cpp $(LIB)/factor_base.o
	$(CC) $(ERR) $(DBG) $(TST)/try_factor_base.cpp $(LIB)/factor_base.o $(LIB)/erat_sieve.o -o $(TSE)/try_factor_base $(GMP)

$(TSE)/try_erat_sieve: $(TST)/try_erat_sieve.cpp $(UTL)/erat_sieve.cpp
	$(CC) $(ERR) $(DBG) $(TST)/try_erat_sieve.cpp $(LIB)/erat_sieve.o -o $(TSE)/try_erat_sieve 

$(TSE)/try_serial_sieve: $(TST)/try_serial_sieve.cpp $(LIB)/serial_sieve.o $(LIB)/factor_base.o $(LIB)/qs_vector.o $(LIB)/qs_matrix.o
	$(CC) $(ERR) $(DBG) $(TST)/try_serial_sieve.cpp $(LIB)/erat_sieve.o $(LIB)/serial_sieve.o $(LIB)/factor_base.o $(LIB)/qs_vector.o $(LIB)/poly_eval.o  -o $(TSE)/try_serial_sieve $(LIB)/qs_matrix.o $(GMP) 

$(TSE)/try_qs_vector:  $(TST)/try_qs_vector.cpp $(LIB)/qs_vector.o
	$(CC) $(ERR) $(DBG) $(TST)/try_qs_vector.cpp $(LIB)/qs_vector.o $(LIB)/poly_eval.o -o $(TSE)/try_qs_vector $(GMP)

$(TSE)/try_qs_bin_vector:  $(TST)/try_qs_bin_vector.cpp $(LIB)/qs_bin_vector.o $(LIB)/qs_vector.o
	$(CC) $(ERR) $(DBG) $(TST)/try_qs_bin_vector.cpp $(LIB)/poly_eval.o $(LIB)/qs_bin_vector.o $(LIB)/qs_vector.o -o $(TSE)/try_qs_bin_vector $(GMP)

$(TSE)/try_qs_matrix: $(TST)/try_qs_matrix.cpp $(LIB)/qs_matrix.o
	$(CC) $(ERR) $(DBG) $(TST)/try_qs_matrix.cpp $(LIB)/qs_matrix.o $(LIB)/qs_vector.o $(LIB)/poly_eval.o -o $(TSE)/try_qs_matrix $(GMP)

$(TSE)/try_qs_bin_matrix: $(TST)/try_qs_bin_matrix.cpp $(LIB)/qs_matrix.o $(LIB)/qs_bin_matrix.o
	$(CC) $(ERR) $(DBG) $(TST)/try_qs_bin_matrix.cpp $(LIB)/qs_matrix.o $(LIB)/qs_bin_matrix.o $(LIB)/qs_vector.o $(LIB)/qs_bin_vector.o $(LIB)/poly_eval.o -o $(TSE)/try_qs_bin_matrix $(GMP)

$(TSE)/quadratic_sieve: $(TST)/quadratic_sieve.cpp $(LIB)/qs_matrix.o $(LIB)/qs_bin_matrix.o $(LIB)/serial_sieve.o  $(LIB)/elimination.o
	$(CC) $(ERR) $(DBG) $(TST)/quadratic_sieve.cpp $(LIB)/qs_matrix.o $(LIB)/qs_bin_matrix.o $(LIB)/serial_sieve.o $(LIB)/erat_sieve.o $(LIB)/factor_base.o $(LIB)/qs_vector.o $(LIB)/qs_bin_vector.o $(LIB)/poly_eval.o $(LIB)/elimination.o -o $(TSE)/quadratic_sieve $(GMP)

clean: 
	rm $(LIB)/*

cleantst:
	rm $(TSE)/*